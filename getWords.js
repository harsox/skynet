const request = require('superagent')
const cheerio = require('cheerio')
const fs = require('fs')

const url = numLetters => {
  return `http://www.litscape.com/words/length/${numLetters}_letters/${numLetters}_letter_words.html`
}

const getWords = async numLetters => {
  const {res: {text}} = await request.get(url(numLetters))
  const $ = cheerio.load(text)
  const wordText = $('#wordlistdisplay').text()
  const words = wordText.split(' ')
  fs.writeFileSync(`./imports/words${numLetters}.json`, JSON.stringify(words))
}

;(async () => {
  try {
    for (let letters = 2; letters <= 10; letters++) {
      await getWords(letters)
    }
  } catch (err) {
    console.log(err)
  }
})()