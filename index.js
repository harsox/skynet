// nice site for words
// http://www.litscape.com

// How to choose the number of hidden layers and nodes
// https://stats.stackexchange.com/questions/181/how-to-choose-the-number-of-hidden-layers-and-nodes-in-a-feedforward-neural-netw

const {Neuron, Layer, Network, Trainer, Architect} = require('synaptic')
const chalk = require('chalk')
const prompt = require('prompt')
const fs = require('fs')

const alphabet = 'abcdefghijklmnopqrstuvxyz'.split('')

const MIN_LETTERS = 2
const MAX_LETTERS = 10
const REAL_WORD = 0
const MUTATED_WORD = 1
const FAKE_WORD = 2

const NETWORK_NAME = 'realwordbot'
const NUM_INPUTS = MAX_LETTERS * alphabet.length
const NUM_OUTPUT = 1
const NUM_HIDDEN = (NUM_INPUTS + NUM_OUTPUT) / 2 | 0
const LEARNING_RATE = 0.15

const flatten = (array) => {
  return [].concat.apply([], array)
}

const shuffle = (array) => {
  return array.sort((a, b) => {
    const rand = Math.random()
    return rand < 0.33 ? -1 : rand < 0.66 ? 0 : 1
  })
}

const fullDictionary = flatten(
  new Array(MAX_LETTERS).fill(null).map((_, i) => {
    return i >= MIN_LETTERS ? require('./imports/words' + i) : null
  }).filter(w => w !== null)
)

const exportsFile = () => {
  return `./exports/network-${NETWORK_NAME}-${NUM_INPUTS}-${NUM_HIDDEN}-${NUM_OUTPUT}.json`
}

// MSE
const cost = (target, output) => {
  let mse = 0
  for (let i in output)
    mse += Math.pow(target[i] - output[i], 2)
  return mse / output.length
}

const saveNetwork = network => {
  try {
    const json = JSON.stringify(network.toJSON())
    console.log('Saving network', json.length)
    fs.writeFileSync(exportsFile(), json, 'utf8')
  } catch (err) {
    console.log(err)
  }
}

const networkFromJSON = json => {
  return Network.fromJSON(json)
}

const readJSON = async filename => {
  return new Promise((resolve, reject) => {
    fs.readFile(filename, 'utf8', (err, data) => {
      if (err) {
        resolve(null)
      } else {
        resolve(JSON.parse(data))
      }
    })
  })
}

const wordToInput = word => {
  return flatten(
    new Array(MAX_LETTERS).fill(0).map(
      (_, i) => alphabet.map(letter => letter === word[i] ? 1 : 0)
    )
  )
}

const generateTestData = () => {
  return shuffle(fullDictionary).slice(0, 1000).map(mutateWord)
}

const wordsToTrainingData = words =>
  words.map(({word, isReal}) => ({
    input: wordToInput(word),
    output: [isReal ? 1 : 0]
  }))

const randomLetter = () => {
  return alphabet[Math.random() * alphabet.length | 0]
}

// hello => hexlo, cello, helly
const mutateWordRandom = word => {
  const mutationRate = .3
  const letters = word.split('')
  return letters.map(letter => Math.random() < mutationRate ? randomLetter() : letter).join('')
}

// hello => heelo, hhhlo, heeeo
const mutateWordStretch = word => {
  const mutationRate = .3
  const letters = word.split('')
  return letters.reduce((pre, letter, index) => {
    return pre + (Math.random() < mutationRate ? (index > 0 ? word[index - 1] : randomLetter()) : letter)
  }, '')
}

// hello => llohe, helol, ehllo
const mutateWordScramble = word => {
  const letters = word.split('')
  return shuffle(letters).join('')
}

const mutateWord = (word, mutationFunction) => {
  const newWord = mutationFunction(word)
  const isRealWord = fullDictionary.indexOf(newWord) !== -1
  return isRealWord ? mutateWord(word, mutationFunction) : newWord
}

const fakeWord = () => {
  const word = new Array(MIN_LETTERS + Math.random() * (MAX_LETTERS - MIN_LETTERS) | 0).fill(null).map(randomLetter).join('')
  const isRealWord = fullDictionary.indexOf(word) !== -1
  return isRealWord ? fakeWord() : word
}

const createNetwork = () => {
  const inputLayer = new Layer(NUM_INPUTS)
  const hiddenLayer = new Layer(NUM_HIDDEN)
  const outputLayer = new Layer(NUM_OUTPUT)
  inputLayer.project(hiddenLayer)
  hiddenLayer.project(outputLayer)
  const network = new Network({
    input: inputLayer,
    hidden: [hiddenLayer],
    output: outputLayer
  })
  return network
}

const getRealWord = (words) => {
  return words[Math.random() * words.length | 0]
}

const trainingSession = (network, words, {iterations = Infinity, save, log}) => {
  let totalError = 0
  let lastError = 0
  for (let i = 1; i <= iterations; i++) {
    let word
    const random = Math.random()
    const typeOfWord = random < .5 ? 
      REAL_WORD :
      random < .75 ? FAKE_WORD : 
      MUTATED_WORD

    if (typeOfWord === REAL_WORD) {
      word = getRealWord(words)
    }
    
    if (typeOfWord === FAKE_WORD) {
      word = fakeWord()
    }

    if (typeOfWord === MUTATED_WORD) {
      const random2 = Math.random()
      const realWord = getRealWord(words)
      const mutationFunction = realWord.length <= 3 || random2 < .33 ?
        mutateWordRandom :
        random2 < .66 ? mutateWordStretch :
        mutateWordScramble
      word = mutateWord(realWord, mutationFunction)
    }

    const isRealWord = words.indexOf(word) !== -1    
    const input = wordToInput(word)
    const target = [isRealWord ? 1 : 0]

    const output = network.activate(input)
    network.propagate(LEARNING_RATE, target)

    totalError += cost(target, output)
    const error = totalError / i

    if (i % log === 0) {
      const improvedError = error < lastError
      lastError = error
      const colorGradient = ['red', 'yellow', 'green']
      const wordFixed = word + ' '.repeat(MAX_LETTERS - word.length)
      const wordString = typeOfWord === REAL_WORD ? chalk.green(wordFixed) : typeOfWord === MUTATED_WORD ? chalk.yellow(wordFixed) : chalk.red(wordFixed)
      const guess = Math.round(output[0]) === 1 ? 'Real' : 'Fake'
      const guessString = Math.round(output[0]) === target[0] ? chalk.cyan(guess) : chalk.magenta(guess)
      const realnessColor = chalk[colorGradient[output[0] * colorGradient.length | 0]]
      const realnessString = realnessColor(`[${output[0] * 100 | 0}%]`)
      const errorString = `${error.toFixed(5)} ` + (improvedError ? chalk.green('▼') : chalk.red('▲'))
      console.log(`${i}\t${wordString}\t${guessString}\t${realnessString}\t${errorString}`)
    }

    if (i % save === save - 1) {
      saveNetwork(network)
    }
  }
}

const trainNetwork = (network) => {
  trainingSession(network, fullDictionary, {
    iterations: Infinity, 
    save: 10000, 
    log: 1000
  })
}

const printResult = ({word, output}) => {
  const isInDictionary = fullDictionary.indexOf(word) !== -1
  const realWord = Math.round(output) === 1
  const outputNumber = output[0]
  const rating = !isInDictionary && !realWord ? 
    chalk.yellow('Meh') : 
    isInDictionary && realWord ?
    chalk.yellow('Ultra meh') : 
    !isInDictionary && realWord ?
    chalk.green('New word?') :
    chalk.red('Bad AI')
   console.log(
    word,
    Math.round(output) === 1 ? chalk.green('Real word') : chalk.red('Fake word'),
    output, 
    rating
  )
}

const testNetwork = (network) => {
  prompt.start()
  prompt.get(['word'], (err, {word}) => {
    if (err) {
      throw new Error(err)
    }
    const output = network.activate(wordToInput(word))
    printResult({word, output})
    console.log('')
    testNetwork(network)
  })
  return;
  const results = generateTestData().map(word => {
    const output = network.activate(wordToInput(word))
    return {word, output}
  })
  results.sort((a, b) => b.output[0] - a.output[0]).forEach(printResult)
}

;(async () => {
  try {
    const networkJSON = await readJSON(exportsFile())
    const network = !networkJSON ? createNetwork() : networkFromJSON(networkJSON)
    prompt.start()
    console.log('train/test?')
    prompt.get(['whatDo'], (err, {whatDo}) => {
    if (err) {
      throw new Error(err)
    }
    whatDo === 'test' ? 
      testNetwork(network) :
      trainNetwork(network)
  })
  } catch (err) {
    console.log(err)
  }
})()